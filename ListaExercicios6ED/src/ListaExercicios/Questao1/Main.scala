package ListaExercicios.Questao1

object Principal {

  def main(args: Array[String]): Unit = {
    
    //Item 1
    var set:Set = new Set
    set.createSet(Array("A","E", "I", "O", "U"))

    //Item 2
    set.insertSet("A")
    set.insertSet("E")
    set.insertSet("I")
    set.insertSet("O")
    println("Impressao do Conjunto 1: ")
    set.printSet()

    //Item 3
    set.removeSet("A")
    println("Resultado da remocao de A: ")
    set.printSet()


    //Item 4
    var set2:Set = new Set
    set2.createSet(Array("A","E", "I", "O", "U"))
    set2.insertSet("I")
    set2.insertSet("O")
    set2.insertSet("U")
    
    println("Impressao do Conjunto 1: ")
    set.printSet()
    println("Impressao do Conjunto 2: ")
    set2.printSet()
    
    set2.bits = set.unionSet(set2)
    println("Resultado da Uniao entre os conjuntos 1 e 2: ")
    set2.printSet()

        
    //Item 5
    var set3:Set = new Set
    set3.createSet(Array("A","E", "I", "O", "U"))
    set3.insertSet("U")
    set3.insertSet("A")
    set3.insertSet("I")
    
    println("Impressao do Conjunto 1: ")
    set.printSet()
    println("Impressao do Conjunto 3: ")
    set3.printSet()
    
    set3.bits = (set.intersectionSet(set3))
    println("Resultado da intersecao entre os conjuntos 1 e 3: ")
    set3.printSet()

    //Item 6
    println("Impressao do Conjunto 2: ")
    set2.printSet()
    println("Impressao do Conjunto 3: ")
    set3.printSet()
    
    set2.bits = set2.diferenceSet(set3)
    println("Resultado da  diferenca entre os conjuntos 2 e 3")
    set2.printSet()

    //Item 7
    println("Impressao do Conjunto 1: ")
    set.printSet()
    println("Impressao do Conjunto 2: ")
    set2.printSet()
     println("Impressao do Conjunto 3: ")
    set3.printSet()

    if(set.verifySubSet(set2))
      println("O cojunto 2 e subconjunto de conjunto 1")
    else
      println("O cojunto 2 nao e subconjunto de conjunto 1")
      
    if(set.verifySubSet(set3))
      println("O cojunto 3 e subconjunto de conjunto 1")
    else
      println("O cojunto 3 nao e subconjunto de conjunto 1")
    println()
    
    //Item 8
    println("Impressao do Conjunto 1: ")
    set.printSet()
    println("Impressao do Conjunto 2: ")
    set2.printSet()
    
    if(set.verifyEqualsSet(set2))
      println("Conjuntos iguais\n")
    else
      println("Conjuntos diferentes\n")

    //Item 9
    set3.bits = set3.complementSet()
    println("Resultado do complemento do conjunto 3")
    set3.printSet()

    //Item 10
    println("Impressao do Conjunto 1: ")
    set.printSet()
    if(set.belongToTheSet("I"))
      println("I pertence ao conjunto 1\n")
    else
      println("I nao pertence ao conjunto 1\n")

    //Item 11
    println("Numero de elementos do conjunto 1: ")
    println(set.numberElementsSet())

    //Item 12
    set.freeSet()
    set2.freeSet()
    set3.freeSet()
  }


}
