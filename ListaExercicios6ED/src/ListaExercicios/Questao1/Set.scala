package ListaExercicios.Questao1/**
  * Created by Evilasio Costa Junior on 24/04/2017.
  */
class Set(var values:Array[String] = null, var bits:Int=0) {

  def createSet(values:Array[String])={
    this.values = values
  }

  def insertSet(value:String):Unit={
    for(i<- 0 until values.length){
      if(values(i).==(value)){
        turnonBit(i)
        return
      }
    }
  }

  def removeSet(valor:String): Unit ={
    for(i<- 0 until values.length){
      if(values(i).==(valor)){
        turnoffBit(i)
        return
      }
    }
  }

  def unionSet(set:Set): Int = {
    return set.bits | this.bits
  }

  def intersectionSet(set:Set): Int = {
    return set.bits & this.bits
  }

  def diferenceSet(set:Set): Int = {
    set.bits.<(bits) match { 
      case true => return this.bits - set.bits
      case false => return set.bits - this.bits
    }
  }

  def verifySubSet(set:Set): Boolean ={
    if(intersectionSet(set) == set.bits && unionSet(set) == this.bits)
      return true
    return false
  }

  def verifyEqualsSet(set:Set): Boolean = {
    return (this.bits == set.bits)
  }

  def complementSet(): Int ={
    return ~this.bits
  }

  def belongToTheSet(value:String): Boolean ={
    for(i<- 0 until values.length){
      if(values(i).==(value)){
        return online(i)
      }
    }
    return false
  }

  def numberElementsSet(): Int ={
    var value = bits
    var bit = 0
    var count = 0
    while(value!= 0){
      bit = value%2
      if(bit == 1) count += 1
      value = value/2
    }
    return count
  }

  def freeSet() ={
    values = null
    bits = 0
  }

  def turnonBit(i:Int): Unit ={
    if(!online(i))
      bits += Math.pow(2,i).toInt
  }

  def turnoffBit(i:Int): Unit ={
    if(online(i))
      bits -= Math.pow(2, i).toInt
  }

  def online(i:Int): Boolean ={
    var mask = 1 << i
    return ((mask & bits) != 0)
  }

  def printSet(): Unit ={
    for(i<- 0 until values.length){
      if(online(i))
        println(values(i))
    }
    println()
  }





}
